package com.glearning.orders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import com.glearning.orders.model.Order;
import com.glearning.orders.repository.OrderJpaRepository;

@Configuration
public class ApplicationConfig implements CommandLineRunner{

	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private OrderJpaRepository orderRepository;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("-------------------------");
		String [] beans = this.applicationContext.getBeanDefinitionNames();
		int id = 1;
		for (String bean: beans) {
			
			Order order = new Order();
			order.setId(id);
			order.setName(bean);
			order.setPrice(bean.length() * 443);
			this.orderRepository.save(order);
			id++;
			//System.out.println(bean);
		}
		//System.out.println("Total number of beans :: "+beans.length);
		System.out.println("-------------------------");
		
	}
	
	

}
