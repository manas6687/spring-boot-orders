package com.glearning.orders.service;

import java.util.Set;

import com.glearning.orders.model.Order;

public interface OrderService {
	
	Order save(Order order);
	
	Set<Order> fetchAll();
	
	Order fetchOrderById(int id);
	
	void deleteOrderById(int id);
}