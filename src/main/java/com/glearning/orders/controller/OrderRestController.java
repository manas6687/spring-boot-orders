package com.glearning.orders.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.glearning.orders.model.Order;
import com.glearning.orders.service.OrderService;

@RestController
@RequestMapping("/api/v1/orders")
public class OrderRestController {
	
	@Autowired
	private OrderService orderService;
	
	public Order saveOrder(Order order) {
		return this.orderService.save(order);
	}
	
	@GetMapping
	public Set<Order> fetchOrders(){
		return this.orderService.fetchAll();
	}
	
	@GetMapping("/{orderId}")
	public Order fetchOrderById(@PathVariable("orderId") int id) {
		return this.orderService.fetchOrderById(id);
	}
	
	@DeleteMapping("/{id}")
	public void deleteOrderById(@PathVariable int id) {
		this.orderService.deleteOrderById(id);
	}
}